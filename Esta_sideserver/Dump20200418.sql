-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: tmexiot_db
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_esta_conf`
--

DROP TABLE IF EXISTS `tbl_esta_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_esta_conf` (
  `conf_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `conf_name` varchar(45) DEFAULT NULL,
  `conf_lugar` varchar(45) DEFAULT NULL,
  `conf_costo1` float DEFAULT NULL,
  `conf_costo2` float DEFAULT NULL,
  `conf_fecha` datetime DEFAULT NULL,
  `conf_vigencia` int(11) DEFAULT '1',
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_esta_conf`
--

LOCK TABLES `tbl_esta_conf` WRITE;
/*!40000 ALTER TABLE `tbl_esta_conf` DISABLE KEYS */;
INSERT INTO `tbl_esta_conf` VALUES (1,'name','place',12,420,'2020-01-12 21:37:35',0),(2,'abc','NULL',420,420,NULL,0),(3,'a','NULL',1,2,NULL,0),(4,'Pachecos','NULL',13,79,NULL,0),(5,'Rastaman GanJAH','NULL',25,142,NULL,0),(6,'Rastas','NULL',12,55,NULL,0),(7,'Ras Ras','NULL',14,49,NULL,0),(8,'Ras','NULL',25,98,NULL,0),(9,'a','NULL',1,1,NULL,0),(10,'a','NULL',1,1,NULL,0),(11,'a','NULL',1,1,NULL,0),(12,'a','NULL',1,1,NULL,0),(13,'a','NULL',1,1,NULL,0),(14,'a','NULL',1,1,NULL,0),(15,'a','NULL',1,1,NULL,0),(16,'Estacionamiento','NULL',12,45,NULL,0),(17,'y','NULL',3,3,NULL,0),(18,'a','NULL',4.2,42,NULL,0),(19,'a','NULL',2,2,NULL,0),(20,'a','NULL',2,2,NULL,1);
/*!40000 ALTER TABLE `tbl_esta_conf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_esta_conv`
--

DROP TABLE IF EXISTS `tbl_esta_conv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_esta_conv` (
  `conv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `conv_name` varchar(45) DEFAULT NULL,
  `conv_costo` float DEFAULT NULL,
  `conv_note` varchar(100) DEFAULT NULL,
  `conv_fecha` date DEFAULT NULL,
  `conv_tipo` int(11) DEFAULT NULL,
  `conv_vigencia` int(11) DEFAULT '1',
  `conv_nvigencia` date DEFAULT NULL,
  PRIMARY KEY (`conv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_esta_conv`
--

LOCK TABLES `tbl_esta_conv` WRITE;
/*!40000 ALTER TABLE `tbl_esta_conv` DISABLE KEYS */;
INSERT INTO `tbl_esta_conv` VALUES (1,'name',450,'sncsdh c\n','2020-01-12',1,0,'2020-02-12'),(2,'name2',12.5,'\n','2020-01-12',0,0,'2020-02-12'),(3,'name3',152,'\n','2020-01-12',1,0,'2020-02-12'),(4,'a',13,'\n','2020-02-01',0,0,'2020-02-12'),(5,'a',12,'\n','2020-02-01',0,1,NULL),(6,'a',12,'\n','2020-02-01',1,0,'2020-02-12'),(7,'a',40,'\n','2020-02-01',1,0,'2020-02-12'),(8,'Ras',800,'jeje\n','2020-02-01',1,0,'2020-02-12'),(9,'Empresa j',25,'\n','2020-02-08',0,0,'2020-02-08'),(11,':v :v',1,'\n','2020-02-08',0,0,'2020-02-12'),(12,':v  :v',2,'\n','2020-02-08',0,0,'2020-02-12'),(13,'sss',1,'\n','2020-02-08',0,0,'2020-02-12'),(14,'q',2,'\n','2020-02-08',0,0,'2020-02-12'),(15,'d',6,'\n','2020-02-08',0,0,'2020-02-12'),(16,'aaaaa',444,'\n','2020-02-08',0,0,'2020-02-12'),(17,'aaa',1,'\n','2020-02-08',0,0,'2020-02-12'),(18,'dad',2,'\n','2020-02-08',0,0,'2020-02-12'),(19,'er2',21,'\n','2020-02-08',0,0,'2020-02-12'),(20,'otra mas',12,':v \n','2020-02-09',1,0,'2020-02-12'),(21,'a',4,'\n','2020-02-12',0,0,'2020-02-12'),(22,'b',8,'\n','2020-02-12',1,0,'2020-02-12'),(23,'c',12,'\n','2020-02-12',0,0,'2020-02-12'),(24,'aa',1,'\n','2020-02-12',0,1,NULL),(25,'bb',2,'\n','2020-02-12',0,1,NULL),(26,'cc',3,'\n','2020-02-12',0,0,'2020-02-12'),(27,'a',2,'\n','2020-03-01',0,1,NULL),(28,'a',4,'\n','2020-03-01',0,1,NULL),(29,'Empresas unid',25,'Con id\n','2020-03-01',1,0,'2020-03-01'),(30,'Otro',25,'otro mas\n','2020-03-01',0,1,NULL),(31,'Otro ',89,'No mms wee\n','2020-03-01',1,1,NULL),(32,'swd',25.5,'\n','2020-03-01',0,0,'2020-03-01');
/*!40000 ALTER TABLE `tbl_esta_conv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_esta_reg`
--

DROP TABLE IF EXISTS `tbl_esta_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_esta_reg` (
  `reg_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reg_CB` varchar(45) DEFAULT NULL,
  `reg_name` varchar(45) DEFAULT NULL,
  `reg_phone` varchar(45) DEFAULT NULL,
  `reg_model` varchar(45) DEFAULT NULL,
  `reg_color` varchar(45) DEFAULT NULL,
  `reg_placa` varchar(45) DEFAULT NULL,
  `reg_pension` varchar(45) DEFAULT NULL,
  `reg_costo` varchar(45) DEFAULT NULL,
  `reg_pagado` varchar(45) DEFAULT NULL,
  `reg_checkin` datetime DEFAULT NULL,
  `reg_checkout` datetime DEFAULT NULL,
  `reg_comment` varchar(100) DEFAULT NULL,
  `reg_costoT` float DEFAULT NULL,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_esta_reg`
--

LOCK TABLES `tbl_esta_reg` WRITE;
/*!40000 ALTER TABLE `tbl_esta_reg` DISABLE KEYS */;
INSERT INTO `tbl_esta_reg` VALUES (1,'151202015534HJCMND06','Edgar Ramos','3311607633','Mustang ','Rojo','HJCMND06','0','15','C','2020-01-15 15:53:04','2020-01-15 21:28:42','Noo pagó',25),(2,'1512020155357Rame921008','Rastas','3311607633','Mustang','Rojo','Rame921008','1','100','C','2020-01-15 15:53:57','2020-01-15 17:34:19','Se fue',20),(3,'1512020155444rame921008hjcmnd06','Rastudito','3311607633','Mercedes-Benz','Plata','rame921008hjcmnd06','1','100','C','2020-01-15 15:54:44','2020-01-15 21:36:26',NULL,NULL),(4,'15120201631HJCMND01','Edgar Ramos','3311607633','Mustang ','Rojo','HJCMND01','1','152.0','C','2020-01-15 16:03:01','2020-01-15 20:58:43',NULL,NULL),(5,'15120201638HJCMND08','Edgar Ramos','3311607633','Mustang ','Rojo','HJCMND08','1','450.0','SI','2020-01-15 16:03:08','2020-01-15 17:40:30',NULL,45),(6,'151202020321899##','No_name','No_phone','null','null','99##','1','100','C','2020-01-15 20:32:18','2020-01-15 21:35:46',NULL,NULL),(7,'161202075111Fulano1234','Fulano Perez','','','','Fulano1234','1','450.0','SI','2020-01-16 07:51:11','2020-02-12 17:29:49',NULL,12600),(8,'16120207535Jurado1234','Juan Jurado','','','','Jurado1234','1','152.0','SI','2020-01-16 07:53:05','2020-02-17 10:14:14',NULL,5016),(9,'161202075454Elma1234','Elizabet María','','','','Elma1234','1','100','C','2020-01-16 07:54:54','2020-02-17 10:09:40','La clienta amenazó al chalan ',0),(10,'161202075516GilA1234','Gilberto Aceves','','','','GilA1234','0','15','C','2020-01-16 07:55:16','2020-02-17 10:32:27','Cliente intentó agredirnos',0),(11,'161202075545Jupe1234','Juan Perez','','','','Jupe1234','0','15','NO','2020-01-16 07:55:45','0000-00-00 00:00:00',NULL,NULL),(12,'16120207560LuisA1234','Luis Aleberto','','','','LuisA1234','0','15','SI','2020-01-16 07:56:00','2020-02-12 17:28:20',NULL,9870),(13,'161202075614Alemo1234','Alejandra Moreno','','','','Alemo1234','0','15','C','2020-01-16 07:56:14','2020-02-17 10:11:29','Cliente solo traia con sigo 10,000',10000),(14,'161202075637Dama1234','Damian Marley','','','','Dama1234','0','15','NO','2020-01-16 07:56:37','0000-00-00 00:00:00',NULL,NULL),(15,'161202075656Julpe1234','Julia Perez','','','','Julpe1234','0','15','NO','2020-01-16 07:56:56','0000-00-00 00:00:00',NULL,NULL),(16,'161202075713DaMon1234','Danira Monclova','','','','DaMon1234','0','15','NO','2020-01-16 07:57:13','0000-00-00 00:00:00',NULL,NULL),(17,'161202075729Juame1234','Juana Medina','','','','Juame1234','0','15','NO','2020-01-16 07:57:29','0000-00-00 00:00:00',NULL,NULL),(18,'161202075750Raul1234','Raul Perez','','','','Raul1234','0','15','C','2020-01-16 07:57:50','2020-02-17 09:59:00',NULL,11565),(19,'161202075832Palacios1234','Raul Palacios','','','','Palacios1234','0','12.5','C','2020-01-16 07:58:32','2020-02-17 09:57:23',NULL,9625),(20,'16120207592LuisAl1234','Luisa Alvarez','','','','LuisAl1234','1','100','NO','2020-01-16 07:59:02','0000-00-00 00:00:00',NULL,NULL),(21,'161202075930LeoBa1234','Leonarda Genoveba','','','','LeoBa1234','1','100','C','2020-01-16 07:59:30','2020-02-16 17:56:36',NULL,3200),(22,'16120208035RaRa1234','Ramira Ramona','','','','RaRa1234','0','12.5','C','2020-01-16 08:00:35','2020-02-12 17:31:57',NULL,NULL),(24,'182202021183Raro222','Ramon Roman','2224258745','','','Raro222','0','2.0','SI','2020-02-18 21:18:04','2020-02-18 21:33:57',NULL,2),(25,'182202021213ube1248','Vato mmn udla','','','','ube1248','1','100','SI','2020-02-18 21:21:04','2020-02-18 21:32:51',NULL,100),(26,'2222020191532Juan2468','Juan PP','33333333','','','Juan2468','0','12.0','SI','2020-02-22 19:15:32','2020-02-22 19:17:06',NULL,12),(27,'2322020145832placa2222','Cliente','','','','placa2222','1','100','NO','2020-02-23 14:58:33',NULL,NULL,NULL),(28,'2322020153739otro323','Otro','323232','llii','','otro323','1','100','NO','2020-02-23 15:37:39',NULL,NULL,NULL),(29,'2322020154138otro323','Otro','323232','llii','','otro323','0','15','NO','2020-02-23 15:41:39',NULL,NULL,NULL),(30,'2322020154422rara25','Rasputin','258258','','','rara25','0','15','NO','2020-02-23 15:44:23',NULL,NULL,NULL),(31,'2322020154438rara25','RasputinII','258258','','','rara25','0','1.0','C','2020-02-23 15:44:38','2020-02-23 21:42:09','Solo traia 1 peso',1),(32,'23220201551355aa','All','','','','55aa','0','12','SI','2020-02-23 15:51:04','2020-02-23 17:24:22',NULL,24),(33,'2322020155227Ano69','Another one','','','','Ano69','1','45.0','NO','2020-02-23 15:52:28',NULL,NULL,NULL),(34,'2322020172634o2','Other2','','','','o2','0','12.0','NO','2020-02-23 17:26:35',NULL,NULL,NULL),(35,'2322020172645q2','q2','','','','q2','0','2.0','NO','2020-02-23 17:26:46',NULL,NULL,NULL),(36,'2322020204052aaaaa','a','','','','aaaaa','0','12.0','NO','2020-02-23 20:40:52',NULL,NULL,NULL),(37,'2322020204133iioo','aa','ww','ee','ggii','iioo','0','12.0','NO','2020-02-23 20:41:33',NULL,NULL,NULL),(38,'2322020205123aaaaa','a','a','a','a','aaaaa','1','45.0','NO','2020-02-23 20:51:23',NULL,NULL,NULL),(39,'2322020205555dded','A','21212','a','d','dded','0','2.0','NO','2020-02-23 20:55:56',NULL,NULL,NULL),(40,'232202021433asasa1','','','','','asasa1','1','45.0','NO','2020-02-23 21:04:33',NULL,NULL,NULL),(41,'2322020211040qwqw22','','','','','qwqw22','0','12.0','NO','2020-02-23 21:10:40',NULL,NULL,NULL),(42,'1432020113616a','a','a','a','a','a','1','2.0','NO','2020-03-14 11:36:17',NULL,NULL,NULL),(43,'1432020113735a','a','a','a','a','a','0','2.0','NO','2020-03-14 11:37:35',NULL,NULL,NULL),(44,'1432020113819a','a','a','a','a','a','0','2.0','NO','2020-03-14 11:38:19',NULL,NULL,NULL),(45,'1432020113933a','a','a','a','a','a','0','2.0','NO','2020-03-14 11:39:34',NULL,NULL,NULL),(46,'143202011421a','a','a','a','a','a','0','2.0','NO','2020-03-14 11:42:01',NULL,NULL,NULL),(47,'1432020114346a','a','a','a','a','a','0','2.0','NO','2020-03-14 11:43:47',NULL,NULL,NULL),(48,'1432020114447a','a','a','a','a','a','1','2.0','NO','2020-04-17 11:44:48',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_esta_reg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tmexiot_db'
--

--
-- Dumping routines for database 'tmexiot_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-18 13:42:26
